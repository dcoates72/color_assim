from psychopy import data, visual, core, event
import random # os  datetime, itertools, csv
import numpy as np

import sys
sys.path.append('../clab_core')
import stereocore
import midicore

#col1=[0.75,0.75,0]
#col2=[0,0,1]

# Nice R+G and orange
#col1=[0,0.5,0]
#col2=[0.8,0,0]

# Nice R+G and orange
#col1=[0,0.4,0]
#col2=[0.7,0,0]

col1=[0.0,0.5,0.0]
col2=[0.7,0.0,0.0]

col_mix1=[0.6,0.35,0.0]
col_mix2=[0.6,0.35,0.0]

# Fix color: turn proportions (0-1) into Psychopy #s
def fixcol(r,g,b):
    return (r*2-1,g*2-1,b*2-1)

ck_size=4
nrects=20
spacing=ck_size*2
offset=ck_size/2.0

height=200

def process_midi():
    global col1,col2,col_mix1,col_mix2,done
    msg=midi.poll_knobs() 
    while not (msg==None):
            if msg[0]==midicore.LC_1_top:
                val=msg[1]/float(midicore.MIDI_max)
                col1=[0.0,val,0.0]
            if msg[0]==midicore.LC_1_btm:
                val=msg[1]/float(midicore.MIDI_max)
                col2=[val,0.0,0.0]
            if msg[0]==midicore.LC_2_top:
                val=msg[1]/float(midicore.MIDI_max)
                col_mix1=[val,col_mix1[1],0.0]
                col_mix2=[val,col_mix1[1],0.0]
            if msg[0]==midicore.LC_2_btm:
                val=msg[1]/float(midicore.MIDI_max)
                col_mix1=[col_mix1[1],val,0.0]
                col_mix2=[col_mix1[1],val,0.0]
            if msg[0]==midicore.LC_btn_1:
                done=True
            msg=midi.poll_knobs() 
    
if __name__=="__main__":
    stereo_win=stereocore.PROPixx () #test_mono()
    winL,winR=stereo_win.setup_win(
            units="pix",
            fullscr=True,
           # blendMode='avg', 
            color='black'
            #winType="pyglet"
    )

    centerxL=-600
    centerxR=400
    
    midi=midicore.midi_control()
    midi.init_midi()

    done=False
    
    lines_green = [visual.Rect(winL, size=[ck_size,ck_size*height], fillColor=fixcol(*col1),
                        lineColor=None, pos=(-nrects*ck_size/2+spacing*x+ck_size+centerxL,0) ) for x in range(nrects) ]
    
    lines_red = [visual.Rect(winR, size=[ck_size,ck_size*height], fillColor=fixcol(*col2),
                        lineColor =None, pos=(-nrects*ck_size/2+spacing*x+ck_size+offset+centerxL,0) ) for x in range(nrects) ]
    
    lines_mix1 = [visual.Rect(winL, size=[ck_size,ck_size*height], fillColor=fixcol(*col_mix1),
                        lineColor=None, pos=(-nrects*ck_size/2+spacing*x+ck_size+centerxR,0) ) for x in range(nrects) ]
    
    lines_mix2 = [visual.Rect(winR, size=[ck_size,ck_size*height], fillColor=fixcol(*col_mix2),
                        lineColor =None, pos=(-nrects*ck_size/2+spacing*x+ck_size+offset+centerxR,0) ) for x in range(nrects) ]


    fixC=[0,0,0]
    line_fixL = [visual.Line(winL, start=(centerxL+lineoff,-ck_size*height+lineoff),
                                     end=(centerxL+lineoff+200, ck_size*height+lineoff),
                            fillColor=None, lineColor=fixC) for lineoff in np.linspace(-200,0,10) ]
    line_fixR = [visual.Line(winR, start=(centerxL+lineoff,-ck_size*height+lineoff),
                                     end=(centerxL+lineoff+200, ck_size*height+lineoff),
                            fillColor=None, lineColor=fixC) for lineoff in np.linspace(-200,0,10) ]
   
    #line_fixL= []
    #line_fixR= []
    
    while not done:
        process_midi()

        [line1.setFillColor(fixcol(*col1)) for line1 in lines_green]
        # Green ones
       # for x in range(rects):

#                stereo_win.draw([arect],'left',doflip=False)
#    
#                arect=visual.Rect(winL, size=[ck_size,ck_size*height], fillColor=fixcol(*col_mix1),
#                        lineColor=None, pos=(-rects*ck_size/2+spacing*x+ck_size+centerxR,0) )
#                stereo_win.draw([arect],'left',doflip=False)

        stereo_win.draw(lines_green+line_fixL+lines_mix1,'left') # now flip
                
        [line1.setFillColor (fixcol(*col2)) for line1 in lines_red]

        # Red ones
#        for x in range(rects):
#
#    
#                arect=visual.Rect(winR, size=[ck_size,ck_size*height], fillColor=fixcol(*col_mix2),
#                        lineColor=None, pos=(-rects*ck_size/2+spacing*x+ck_size+offset+centerxR,0) )
#                stereo_win.draw([arect],'right',doflip=False)
                
        process_midi()
        stereo_win.draw(lines_red+line_fixR+lines_mix2,'right') # now flip

        keys = event.getKeys(keyList=['space', 'escape'])
        done = done or len(keys)>0 # "done or" to allow midi quit (above) to pass through

    del stereo_win


